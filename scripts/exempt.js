var $ = jQuery;

var certLink = null;
var ajaxLoad = null;
var reloadWithSave = null;
var certSelectUrl = null;
var certDeSelectUrl = null;
var merchantNameForCert = null;
var saveCertUrl = null;
var certListUrl = null;
var certRemoveUrl = null;
var withConfirm = null;
var hiddenCertificateField = null;

function deselect_certificate() {
	$.get(certDeSelectUrl, null, function (data) { window.location.reload(); });
}

jQuery(document).ready(function() {

	$('#uc_taxcloud_deselect').click(function() { deselect_certificate(); });

	certLink = 'xmptlink';
	ajaxLoad = true;
	reloadWithSave = true;
	merchantNameForCert = Drupal.settings.uc_taxcloud.merchantNameForCert;
	//NOTE these could/should all be the same URL, but not necessary.
	certSelectUrl = Drupal.settings.uc_taxcloud.certSelectUrl;
	certDeSelectUrl = Drupal.settings.uc_taxcloud.certDeSelectUrl;
	saveCertUrl = Drupal.settings.uc_taxcloud.saveCertUrl;//Save a new exemption cert
	certListUrl = Drupal.settings.uc_taxcloud.certListUrl; //list existing exemption certs for customer
	//certListUrl = 'http://taxcloud.net/imgs/cert/sample_cert_list.aspx'; //list existing exemption certs for customer
	certRemoveUrl = Drupal.settings.uc_taxcloud.certRemoveUrl; //Note: this will be called asynchronosly (no page refresh);
	//reload the cart/checkout page after selecting a certificate (will force a new sales tax lookup with the exemption cert applied so rate will return zero)
	//if set to false, the script will not ask the customer to reload.
	withConfirm = true;
	//use this to pass the certificate id to the server for any reason
	hiddenCertificateField = "taxcloud_exemption_certificate";
	//Please do not edit the following line.
	var clearUrl = "?time="+new Date().getTime().toString(); // prevent caching
	(function () {
	    var tcJsHost = (("https:" == document.location.protocol) ? "https:" : "http:"); var ts = document.createElement('script'); ts.type = 'text/javascript'; ts.async = true;
	    ts.src = tcJsHost + '//taxcloud.net/imgs/cert.min.js' + clearUrl; var t = document.getElementsByTagName('script')[0]; t.parentNode.insertBefore(ts, t);
	})();
	
});