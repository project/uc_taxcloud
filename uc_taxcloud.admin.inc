<?php

/**
 * @file
 * Admin settings for uc_taxcloud.
 * License: https://taxcloud.net/ftpsl.pdf
 */

/**
 * Admin settings menu callback.
 */
function uc_taxcloud_admin_settings() {

  $form = array();

  // Set default tax rate and location code.

  $form['uc_taxcloud_default_tic'] = array(
    '#title' => t('Default Taxability Information Code'),
    '#description' => t('The default Taxability Information Code to use, just in case not each product has one defined. See README.txt for instructions on how to configure product content type.'),
    '#type' => 'textfield',
    '#required' => TRUE, 
    '#default_value' => variable_get('uc_taxcloud_default_tic', '00000'),
  );
  
  $form['uc_taxcloud_shipping_tic'] = array(
    '#title' => t('Shipping Taxability Information Code'),
    '#description' => t('The default Taxability Information Code to use for shipping, usually 11010.'),
    '#type' => 'textfield',
    '#required' => TRUE, 
    '#default_value' => variable_get('uc_taxcloud_shipping_tic', '11010'),
  );

  $form['uc_taxcloud_api_id'] = array(
    '#title' => t('Taxcloud API ID'),
    '#description' => t('The ID from your merchant account.'),
    '#type' => 'textfield',
    '#required' => TRUE, 
    '#default_value' => variable_get('uc_taxcloud_api_id', ''),
  );

  $form['uc_taxcloud_api_key'] = array(
    '#title' => t('Taxcloud API KEY'),
    '#description' => t('The KEY from your merchant account.'),
    '#type' => 'textfield',
    '#required' => TRUE, 
    '#default_value' => variable_get('uc_taxcloud_api_key', ''),
  );

  $form['uc_taxcloud_usps_id'] = array(
    '#title' => t('USPS ID'),
    '#description' => t('Your USPS ID, used to verify address on lookup.') . '<a href="https://secure.shippingapis.com/registration" target="_blank">' . t('You can get one here') . '</a>',
    '#type' => 'textfield',
    '#required' => TRUE, 
    '#default_value' => variable_get('uc_taxcloud_usps_id', ''),
  );

  $form['uc_taxcloud_deliveredbyseller'] = array(
    '#title' => t('Delivered by seller'),
    '#description' => t("How the product was delivered (1= seller's vehicle, 0 = other method i.e UPS, FedEx, etc)"),
    '#type' => 'textfield',
    '#required' => TRUE, 
    '#default_value' => variable_get('uc_taxcloud_deliveredbyseller', '0'),
  );

  $form['uc_taxcloud_store_configure_link'] = array(
    '#type' => 'markup',
    '#markup' => t('Important: If you did not do it yet, you need to') . ' ' . l(t('configure store address here'), 'admin/store/settings/store'), 
  );

  return system_settings_form($form);
}