<?php
module_load_include('inc', 'uc_taxcloud', 'includes/classes');

class TaxCloud {
	
	private $client;
	function __construct() {
		$this->client = new SoapClient("https://api.taxcloud.net/1.0/TaxCloud.asmx?wsdl");
	}

	/**
	 * TaxCloud code examples
	 *
	 * The basic process is:
	 *
	 * 1. Verify your store's address as well as the customer's shipping address through the address verification service
	 *
	 * 2. Call the lookup service to look up the tax amount for each item in the cart
	 *
	 * 3. When the customer completes the transaction call the authorized and capture web service to complete the transaction
	 *
	 * This code assumes that you have already set up a Merchant account at www.TaxCloud.net
	 */
	
	/**
	 * Verify an address using TaxCloud service. Addresses need to be verified through this service
	 * before they are used for other web service calls since they require the complete 9 digit zip code
	 * to look up the tax accurately. 
	 *
	 * @param $address
	 * @param $err
	 */
	function uc_taxcloud_verify_address($address, &$err) {
	
		// Verify the address through the TaxCloud verify address service
		$params = array(
			"uspsUserID" => variable_get('uc_taxcloud_usps_id', ''),
			"address1" => $address->getAddress1(),
			"address2" => $address->getAddress2(),
			"city" => $address->getCity(),
			"state" => $address->getState(),
			"zip5" => $address->getZip5(),
			"zip4" => $address->getZip4());
	
		try {
	
			$verifyaddressresponse = $this->client->verifyAddress( $params );
	
		} catch (Exception $e) {
	
			//retry in case of timeout
			try {
				$verifyaddressresponse = $this->client->verifyAddress( $params );
			} catch (Exception $e) {
	
				$err[] = "Error encountered while verifying address ".$address->getAddress1().
				" ".$address->getState()." ".$address->getCity()." "." ".$address->getZip5().
				" ".$e->getMessage();
				//irreparable, return
				return null;
			}
		}
	
		if($verifyaddressresponse->{'VerifyAddressResult'}->ErrNumber == 0) {
			// Use the verified address values
			$address->setAddress1($verifyaddressresponse->{'VerifyAddressResult'}->Address1);
			$address->setAddress2(isset($verifyaddressresponse->{'VerifyAddressResult'}->Address2) ? $verifyaddressresponse->{'VerifyAddressResult'}->Address2 : NULL);
			$address->setCity($verifyaddressresponse->{'VerifyAddressResult'}->City);
			$address->setState($verifyaddressresponse->{'VerifyAddressResult'}->State);
			$address->setZip5($verifyaddressresponse->{'VerifyAddressResult'}->Zip5);
			$address->setZip4($verifyaddressresponse->{'VerifyAddressResult'}->Zip4);
	
		} else {
	
			$err[] = "Error encountered while verifying address ".$address->getAddress1().
				" ".$address->getState()." ".$address->getCity()." "." ".$address->getZip5().
				" ".$verifyaddressresponse->{'VerifyAddressResult'}->ErrDescription;
	
			return null;
		}
		return $address;
	}
	
	/**
	 * Look up tax using TaxCloud web services
	 * @param $product
	 * @param $origin
	 * @param $destination
	 * @param $shipping
	 * @param $errMsg
	 */
	function uc_taxcloud_lookup_tax($products, $origin, $destination, $shipping, &$errMsg) {
	
		if(is_null($origin)) return -1;
	
		// These address checks are sometimes needed to ensure that the user has logged in. This may not be necessary for your cart.
		if(is_null($destination)) return -1;  
	
		if(!is_null($origin) && !is_null($destination)) {
			$cartItems = Array();
			$index = 0;
			foreach ($products as $k => $product) {
	
				$cartItem = new CartItem();
	
				$cartItem->setItemID($product['productid']); //change this to how your cart stores the product ID
				$cartItem->setIndex($index); // Each cart item must have a unique index
				$tic = $product['tic'];
				if(!$tic) {
					//no TIC has been assigned to this product, use default
					$tic = "00000";
				}
	
				$cartItem->setTIC($tic);
				$price = $product['price']; // change this to how your cart stores the price for the product
				$cartItem->setPrice($price); // Price of each item
				$cartItem->setQty($product['qty']); // Quantity - change this to how your cart stores the quantity
				$cartItems[$index] = $cartItem;
				$index++;
			}
	
			//Shipping as a cart item - shipping needs to be taxed
			$cartItem = new CartItem();
			$cartItem->setItemID('shipping');
			$cartItem->setIndex($index);
			$cartItem->setTIC(variable_get('uc_taxcloud_shipping_tic', '10010'));
			$cartItem->setPrice($shipping);  // The shipping cost from your cart
			$cartItem->setQty(1);
			$cartItems[$index] = $cartItem;

			$params = array(
				"apiLoginID" => variable_get('uc_taxcloud_api_id', ''),
				"apiKey" => variable_get('uc_taxcloud_api_key', ''),
				"customerID" => $_SESSION['taxcloud_customer_id'],
				"cartID" => $_SESSION['taxcloud_cartID'],
				"cartItems" => $cartItems,
				"origin" => $origin,
				"destination" => $destination,
				"deliveredBySeller" => (variable_get('uc_taxcloud_deliveredbyseller', '0') ? true : false),
			);
			
			// Here we are loading a stored exemption certificate. You will probably want to allow the user to select from a list
			// of stored exemption certificates and pass that certificate into this method. 
	//		$exemptCert = $this->uc_taxcloud_get_exemption_certificates($customer);
	//		$exemptCert = $exemptCert[0];
			
			if (isset($_SESSION['taxcloud_singlePurchase']) && $_SESSION['taxcloud_singlePurchase']) {
				$selectedCert = unserialize($_SESSION['taxcloud_singlePurchase']);   //one-time certificate	
			} else {
				if (isset($_SESSION['taxcloud_certificate_selected']) && $_SESSION['taxcloud_certificate_selected']) {
					$certificate_id = $_SESSION['taxcloud_certificate_selected'];
					$certificates = $this->uc_taxcloud_get_exemption_certificates($_SESSION['taxcloud_customer_id']);
					$selectedCert = FALSE;
					foreach($certificates as $certificate) {
						if ($certificate->CertificateID == $certificate_id) {
							$selectedCert = $certificate;
							break;
						}
					}
				} else {
					$selectedCert = FALSE;
				}
			} 
			if ($selectedCert) $params["exemptCert"] = $selectedCert;
			//Call the TaxCloud web service
			try {
				$lookupResponse = $this->client->lookup( $params );
			} catch (Exception $e) {
				//retry
				try {
					$lookupResponse = $this->client->lookup( $params );
				} catch (Exception $e) {
					$errMsg[] = "Error encountered looking up tax amount ".$e->getMessage();
					//irreparable, return
					//Handle the error appropriately 
					return -1;
				}
			}

			$lookupResult = $lookupResponse->{'LookupResult'};
	
			if($lookupResult->ResponseType == 'OK' || $lookupResult->ResponseType == 'Informational') {
				$cartItemsResponse = $lookupResult->{'CartItemsResponse'};
				$cartItemResponse = $cartItemsResponse->{'CartItemResponse'};
				$taxes = Array();
				$index = 0;
	
				//response may be an array
				if ( is_array($cartItemResponse) ) {
					foreach ($cartItemResponse as $c) {
						$amount = ($c->TaxAmount);
						$taxes[$index] = $amount;
						$index++;
					}
				} else {
					$amount = ($cartItemResponse->TaxAmount);
					$taxes[0] = $amount;
				}
				
				// Here you will need to determine what to do with the results. Usually you would return
				// the tax array and assign the tax amount to each item in the cart and then recalculate 
				// the totals. Here we are just returning the total tax amount.
	
				return array_sum($taxes);
	
			} else {
				$errMsgs = $lookupResult->{'Messages'};
				foreach($errMsgs as $err) {
					$errMsg[] = "Error encountered looking up tax amount ".$err->{'Message'};
				}
				//Handle the error appropriately 
			}
		} else {
			return -1;
		}
	}
	
	/**
	 * Authorized with Capture
	 * This represents the combination of the Authorized and Captured process in one step. You can 
	 * also make these calls separately if you use a two stepped commit. 
	 * @param $orderID
	 * @param $errMsg
	 */
	function uc_taxcloud_authorized_with_capture($orderID, &$errMsg) {
	
		$result = 0;

		$dup = t("This purchase has already been marked as authorized");
	
		$dateAuthorized = date("Y-m-d") . 'T' . date('H:i:s');
	
		$params = array(
			"apiLoginID" => variable_get('uc_taxcloud_api_id', ''),
			"apiKey" => variable_get('uc_taxcloud_api_key', ''),
			"customerID" => $_SESSION['taxcloud_customer_id'],
			"cartID" => $_SESSION['taxcloud_cartID'],
			"orderID" => $orderID,
			"dateAuthorized" => $dateAuthorized,
			"dateCaptured" => $dateAuthorized
			);
	
		// The authorizedResponse array contains the response verification (Error, OK, ...)
		$authorizedResponse = null;
		try {
			$authorizedResponse = $this->client->authorizedWithCapture( $params );
		} catch (Exception $e) {
			//infrastructure error, try again
			try {
				$authorizedResponse = $this->client->authorizedWithCapture( $params );
				$authorizedResult = $authorizedResponse->{'AuthorizedWithCaptureResult'};
				if ($authorizedResult->ResponseType != 'OK') {
					$msgs = $authorizedResult->{'Messages'};
					$respMsg = $msgs->{'ResponseMessage'};
					//duplicate means the the previous call was good. Therefore, consider this to be good
					if (trim ($respMsg->Message) == $dup) {
						$errMsg = array();
						return 1;
					}
				} else if ($authorizedResult->ResponseType == 'Error') {
					$msgs = $authorizedResult->{'Messages'};
					$respMsg = $msgs->{'ResponseMessage'};
					//duplicate means the the previous call was good. Therefore, consider this to be good
					if (trim ($respMsg->Message) == $dup) {
						$errMsg = array();
						return 1;
					} else {
						$errMsg[] = "Error encountered looking up tax amount ".$respMsg;
						return 0;
					}
				} else {
					return 0;
				}
	
			} catch (Exception $e) {
				//give up
				$errMsg[] = $e->getMessage();
				// Handle this error appropriately
				return 0;
			}
		}
	
		$authorizedResult = $authorizedResponse->{'AuthorizedWithCaptureResult'};
		if ($authorizedResult->ResponseType == 'OK') {
			$errMsg = array();
			return 1;
		} else {
			$msgs = $authorizedResult->{'Messages'};
			$respMsg = $msgs->{'ResponseMessage'};
	
			$errMsg [] = $respMsg->Message;
			
			// Handle this error appropriately
			
			return 0;
		}
		return $result;
	}
	/**
	 * uc_taxcloud_add_exemption_certificate
	 *
	 * Added a completed exemption certificate to the customer's list for this store
	 * @param $exemptionCertificate
	 * @param $customerID
	 */
	function uc_taxcloud_add_exemption_certificate($exemptionCertificate,$customerID) {
		
		$params = array(
			"apiLoginID" => variable_get('uc_taxcloud_api_id', ''),
			"apiKey" => variable_get('uc_taxcloud_api_key', ''),
			"customerID" => $customerID,
			"exemptCert" => $exemptionCertificate 
			);
		
		try {
			$addExemptionResponse = $this->client->addExemptCertificate($params);
			$res = $addExemptionResponse->{'AddExemptCertificateResult'};
			if ($res->ResponseType == 'OK') {
				return $res->CertificateID;
			} else {
				return 0;
			}

		} catch (Exception $e) {
			watchdog('taxcloud',"Certificates ADD fail: " . $e);
			// Handle this error appropriately
			return 0;
		}
	
	}
	
	/**
	 * uc_taxcloud_get_exemption_certificates
	 *
	 * Get a list of exemption certificates for the given customer. 
	 * This list contains blanket and single use certificates. Normally you would only display blanket certificates to the users.
	 * @param $customerID
	 */
	function uc_taxcloud_get_exemption_certificates($customerID) {
		
		$params = array( "apiLoginID" => variable_get('uc_taxcloud_api_id', ''),
						 "apiKey" => variable_get('uc_taxcloud_api_key', ''),
						 "customerID" => $customerID
					 	 );
			
		try {
			$getExemptCertificatesResponse = $this->client->getExemptCertificates( $params );
			$getCertificatesRsp = $getExemptCertificatesResponse->{'GetExemptCertificatesResult'};
			$exemptCertificatesArray = $getCertificatesRsp->{'ExemptCertificates'};

			if (isset($exemptCertificatesArray->{'ExemptionCertificate'})) {
				$exemptCertificates = $exemptCertificatesArray->{'ExemptionCertificate'};	
			} else {
				$exemptCertificates = null;
			}
			
			if (is_array($exemptCertificates)) {
				return $exemptCertificates;
			} else {
				return $exemptCertificatesArray;
			}
			
		} catch (Exception $e) {
			return Array();
		}
		return Array();
	}
	
	/**
	 * uc_taxcloud_delete_exemption_certificate
	 *
	 * Delete a stored exemption certificate for a customer.
	 * @param $certificateID
	 */
	function uc_taxcloud_delete_exemption_certificate($certID) {
		
		$params = array( 
		 	"apiLoginID" => variable_get('uc_taxcloud_api_id', ''),
			"apiKey" => variable_get('uc_taxcloud_api_key', ''),
			"certificateID" => $certID
		);
		
		try {
			$deleteExemptCertificateResponse = $this->client->deleteExemptCertificate( $params );
	
		} catch (Exception $e) {
			return -1;
		}
	}

	function uc_taxcloud_return_order($order_id, $products = array()) {   

		$cartItems = Array();
		$index = 0;
		foreach ($products as $k => $product) {

			$cartItem = new CartItem();

			$cartItem->setItemID($product['productid']); //change this to how your cart stores the product ID
			$cartItem->setIndex($index); // Each cart item must have a unique index
			$tic = $product['tic'];
			if(!$tic) {
				//no TIC has been assigned to this product, use default
				$tic = "00000";
			}

			$cartItem->setTIC($tic);
			$price = $product['price']; // change this to how your cart stores the price for the product
			$cartItem->setPrice($price); // Price of each item
			$cartItem->setQty($product['qty']); // Quantity - change this to how your cart stores the quantity
			$cartItems[$index] = $cartItem;
			$index++;
		}

		$returnDate = date("Y-m-d") . 'T' . date("H:i:s");
		
		$params = array(
			"apiLoginID" => variable_get('uc_taxcloud_api_id', ''),
			"apiKey" => variable_get('uc_taxcloud_api_key', ''),
			"orderID" => $order_id,
			"cartItems" => $cartItems,
			"returnedDate" => $returnDate); 
		
		try {
			$returnResponse = $this->client->Returned($params);
			return $returnResponse;
		} catch (Exception $e) { 
			return 0;
		}
	
	}

}