Software license: https://taxcloud.net/ftpsl.pdf

How to install uc_taxcloud module.

-- INSTALLATION --

1)- Register for a TaxCloud merchant account at https://taxcloud.net/account/register/

2)- Register to obtain an USPS account ID at https://secure.shippingapis.com/registration
		Follow the steps on the email you'll receive to set this USPS account for production usage, or call USPS support. This module wont work if
		USPS account is not on production mode. This account is used to verify addresses.

3)- Having Ubercart installed, install this module as usual, see http://drupal.org/node/70151 for further information.
		This module requires the following modules: (most of them, required by Ubercart)
		Taxes, Store, Payment, Order, Rules, Entity tokens, Entity API, Views, Chaos tools, Product, Image, File, Field, Field SQL storage

-- CONFIGURATION --

4)- Configure TIC Field for product classes: 

		a)- Go to Structure > Content Types > Product > Manage Fields (admin/structure/types/manage/product/fields)

		b)- Add a TIC Code field. Machine name MUST be field_tic_code of type "Text" with "Text field" widget. Set as non-required, set default value to 00000
				and Maximum length 255

		c)- Repeat steps a, b for each product class of your store. Suggestion: use "Add existing field" for each class once you added the field for the first time.
				You can set different default value for TIC code on each class. See https://taxcloud.net/tic/default.aspx for more information.

5)- Go to Store > Configuration > TaxCloud settings (admin/store/settings/uc_taxcloud)

		a)- Set Default Taxability Information Code for store (this is a global setting, just in case not each product has tic configured).
				See https://taxcloud.net/tic/default.aspx for more information.

		b)- Set Shipping Taxability Information Code. This is the TIC applied to shipping. (The standard shipping TIC is 11010)
				See https://taxcloud.net/tic/default.aspx for more information.

		c)- Set TaxCloud API ID, API Key and USPS ID from steps 1, 2
				See https://taxcloud.net/account/api/ for more information
				
		d)- Set Delivered by seller option (1= seller's vehicle, 0 = other method i.e UPS, FedEx, etc)

6)- Configure Store address: go to Store > Configuration > Store > Store Address (admin/store/settings/store) Set all the fields with valid information.

7)- (Optional) Activate TaxCloud cart pane at Store > Configuration > Cart (admin/store/settings/cart). This will let your users to have pre-calculated taxes
		before checkout. Note: On cart pane, shipping taxes are not calculated.

8)- (Optional) Configure TIC codes for your products: Edit your products. You'll find the TIC code field you configured on step #4 on "Product Information" section
		where SKU is. The text field interface will be replaced automatically with TaxCloud's TIC code selector. (javascript enabled required)

9)- Test your configuration by adding some products to the cart and setting real delivery (shipping) addresses on checkout page. You'll find a "Taxes" line item with 
		tax lookup amount. It's recommended that you enable error displaying on admin/config/development/logging for tests. That will show you valuable information 
		if lookup cannot be done for some reason.
		Important: On production sites, you must not show lookup errors to user (admin/config/development/logging) as they may contain sensitive data.